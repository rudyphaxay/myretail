module.exports = function(grunt) {

	grunt.initConfig({
		concat: {
			options: {
				// define a string to put between each file in the concatenated output
				separator: ';'
			},
			dist: {
				// the files to concatenate
				src: [
					'public/core.js',
					'public/components/**/*.js'
				],
				// the location of the resulting JS file
				dest: 'public/app.js'
			}
		},
		uglify: {
			options: {
			},
			my_target: {
					files: {
							'public/app.min.js': ['public/app.js']
					}
			}
		},
		watch: {
				files: ['<%= jshint.files %>'],
				tasks: ['jshint']
		},
		less: {
			development: {
				files: {
					'public/styles/main.css': 'public/styles/less/main.less'
				}
			}
			production: {
				files: {
					'public/styles/main.css': 'public/styles/less/main.less'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');

	grunt.registerTask('default', ['concat', 'uglify', 'less']);
};
