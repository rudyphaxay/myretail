var express = require('express');
var app = express();
var routeProducts = require('./app/routes/products.js');

var port = process.env.PORT || 8080;

app.use('/api', routeProducts);

app.get('/', function(req, res) {
  res.sendFile('public/index.html', {root: __dirname});
});

app.use('/', express.static(__dirname));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));


app.listen(port, function() {
  console.log('Our app is running on http://localhost:' + port);
});