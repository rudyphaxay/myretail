# README #

Live demo: https://myretail-rudy.herokuapp.com/

### Installation ###

* Open terminal and navigate to a directory where you would like to run the app from
* Clone the source code from https://rudyphaxay@bitbucket.org/rudyphaxay/myretail.git
* Run "npm install" to download and install modules
* Run "npm start" to run the application
* Open a browser and go to http://localhost:8080

### How to run test ###

* run "npm test" command
