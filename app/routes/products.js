var express = require('express');
var router = express.Router();
var fs = require('fs');


router.get('/products', function(req, res) {
  fs.readFile('app/models/products/item-data.json', 'utf8', function(err, json) {
    if (err) {
      throw err;
    }
    res.status(200).json(JSON.parse(json));
  });
});

module.exports = router;
