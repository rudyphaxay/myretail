var underscore = angular.module('underscore', []);

underscore.factory('_', ['$window', function() {
  return $window._;
}]);

var app = angular.module('myRetail', [
  'underscore',
  'myRetail.components',
  'myRetail.services'
]);

