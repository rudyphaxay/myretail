(function() {
	'use strict';

	angular
		.module('myRetail.services')
		.factory('ProductService', ProductService);

	ProductService.$inject = ['$http'];

	/* @ngInject */
	function ProductService($http) {
		return {
			get: function () {
				return $http({
					method: 'GET',
					url: '/api/products'
				});
			}
		};
	}
})();
