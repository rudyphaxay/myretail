'use strict';

describe('myRetail.services module', function() {
	beforeEach(module('myRetail.services'));

// TODO: test api data with response from product service

	describe('Product service', function() {
		it('should return valid data', inject(function(ProductService) {
			expect(ProductService.get).toBeTruthy();
		}));
	});
});
