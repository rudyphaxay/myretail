(function() {
  'use strict';

  angular
    .module('myRetail.components')
    .component('productSlider', {
      bindings: {
        images: '<'
      },
      controller: productSliderCtrl,
      templateUrl: '/public/components/productSlider/index.html'
    });

  function productSliderCtrl() {
    var vm = this;
  }

})();
