(function() {
  'use strict';
  angular
    .module('myRetail.components')
      .component('productDetail', {
        bindings: {
          product: '<'
        },
        controller: productDetailCtrl,
        templateUrl: '/public/components/productDetail/index.html'
      });

  productDetailCtrl.$inject = ['$sce'];

  function productDetailCtrl($sce) {
    var vm = this;

    vm.showCartBtn = function() {
      if (vm.product) {
        return parseInt(vm.product.purchasingChannelCode) === 0 || parseInt(vm.product.purchasingChannelCode) === 1;
      }
    };

    vm.showPickupBtn = function() {
      if (vm.product) {
        return parseInt(vm.product.purchasingChannelCode) === 0 || parseInt(vm.product.purchasingChannelCode) === 2;
      }
    };

    vm.sanitizeHtml = function(html) {
      return $sce.trustAsHtml(html);
    };
  }
})();
