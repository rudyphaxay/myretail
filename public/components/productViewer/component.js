(function() {
	'use strict';

	angular
		.module('myRetail.components')
		.component('productViewer', {
			controller: productViewerCtrl,
			templateUrl: '/public/components/productViewer/index.html'
		});

	productViewerCtrl.$inject = ['ProductService'];

	function productViewerCtrl(ProductService) {
		var vm = this;
		vm.product = null;

		function init() {
			ProductService.get()
				.then(function (res) {
					vm.product = res.data.CatalogEntryView[0];
          console.log(vm.product);
				});
		}

		init();
	}
})();
