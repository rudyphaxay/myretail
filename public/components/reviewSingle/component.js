(function() {
  'use strict';

  angular
    .module('myRetail.components')
    .component('reviewSingle', {
      bindings: {
        review: '<'
      },
      controller: reviewSingleCtrl,
      templateUrl: '/public/components/reviewSingle/index.html'
    });

  function reviewSingleCtrl() {
    var vm = this;
    vm.starArr = [];

    function getStars() {
      var starCount = parseInt(vm.review.overallRating);

      _.times(5, function() {
        starCount > 0 ? vm.starArr.push(true) : vm.starArr.push(false);
        starCount--;
      });
    }

    getStars();
  }
})();
