(function() {
  'use strict';

  angular
    .module('myRetail.components')
    .component('productReview', {
      bindings: {
        review: '<'
      },
      controller: productReviewCtrl,
      templateUrl: '/public/components/productReview/index.html'
    });

  function productReviewCtrl() {
    var vm = this;

  }
})();
